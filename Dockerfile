from python:3

ADD transcribe.py /transcribe.py

VOLUME /gcf

WORKDIR /
CMD ["python", "transcribe.py", "/gcf/GCF_000146045.2_R64_rna.fna"]
